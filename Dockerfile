FROM nginx:1.7
MAINTAINER Jake Sanders <jake@livewyer.com>

# Install runit
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update -qq && \
    apt-get -y install curl runit && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

# Install consul-template
RUN curl -L https://github.com/hashicorp/consul-template/releases/download/v0.4.0/consul-template_0.4.0_linux_amd64.tar.gz | tar -C /usr/local/bin --strip-components 1 -zxf -

# Configure runit
ADD nginx.service /etc/service/nginx/run
ADD consul-template.service /etc/service/consul-template/run
RUN chmod +x /etc/service/nginx/run && chmod +x /etc/service/consul-template/run

# Configure nginx / consul-template
RUN rm -v /etc/nginx/conf.d/*
ADD nginx.conf /etc/consul-templates/nginx.conf

RUN mkdir /srv/website && echo "<?php phpinfo();" > /srv/website/index.php && chmod -R 0666 /srv/website

EXPOSE 80
CMD ["/usr/bin/runsvdir", "/etc/service"]
